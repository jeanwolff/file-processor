package com.company.wolff.pojo;

import java.util.List;

public class Venda extends Transaction{

	public Venda(String id, String salesId, String salesName, List<Item> items) {
		super();
		this.id = id;
		this.salesId = salesId;
		this.salesName = salesName;
		this.items = items;
	}

	public Venda() {
	}

	private String id;

	private String salesId;

	private String salesName;

	private List<Item> items;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSalesId() {
		return salesId;
	}

	public void setSalesId(String salesId) {
		this.salesId = salesId;
	}

	public String getSalesName() {
		return salesName;
	}

	public void setSalesName(String salesName) {
		this.salesName = salesName;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItem(List<Item> items) {
		this.items = items;
	}

}
