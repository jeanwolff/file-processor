package com.company.wolff.pojo;

public class Vendedor extends Transaction{

	public Vendedor() {

	}

	public Vendedor(String id, String cpf, String name, Float salary) {
		super();
		this.id = id;
		this.cpf = cpf;
		this.name = name;
		this.salary = salary;
	}

	private String id;

	private String cpf;

	private String name;

	private float salary;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

}
