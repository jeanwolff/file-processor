package com.company.wolff.pojo;

public class Item extends Transaction{

	public Item(String id, String quantity, Float price) {
		super();
		this.id = id;
		this.quantity = quantity;
		this.price = price;
	}

	public Item() {
	}

	private String id;

	private String quantity;

	private float price;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

}
