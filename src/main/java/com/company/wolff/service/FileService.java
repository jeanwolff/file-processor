package com.company.wolff.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.company.wolff.exception.InvalidTransactionException;
import com.company.wolff.factory.ClienteFactory;
import com.company.wolff.factory.TransactionFactory;
import com.company.wolff.factory.VendaFactory;
import com.company.wolff.factory.VendedorFactory;
import com.company.wolff.pojo.Cliente;
import com.company.wolff.pojo.Item;
import com.company.wolff.pojo.Transaction;
import com.company.wolff.pojo.Venda;
import com.company.wolff.pojo.Vendedor;

public class FileService {

	private List<Transaction> transactions;

	public FileService() {
		transactions = new ArrayList<>();
	}

	public void processTransactionFile(List<String> lines) {
		lines.stream().forEach(t -> {
			try {
				filterTransaction(t);
			} catch (InvalidTransactionException e) {
				e.printStackTrace();
			}
		});
	}

	private void filterTransaction(String transactionLine) throws InvalidTransactionException {
		TransactionFactory factory = getFactoryByTransactionID(transactionLine);
		transactions.add(factory.createTransaction(transactionLine));
	}

	private TransactionFactory getFactoryByTransactionID(String transactionLine) throws InvalidTransactionException {
		TransactionFactory factory;
		if (transactionLine.startsWith("001")) {
			factory = new VendedorFactory();
		} else if (transactionLine.startsWith("002")) {
			factory = new ClienteFactory();
		} else if (transactionLine.startsWith("003")) {
			factory = new VendaFactory();
		} else {
			throw new InvalidTransactionException(
					"Transa��o desconhecida.Erro ao processar transa��o: " + transactionLine);
		}
		return factory;
	}

	public String writeReportOut() {
		long customerQty = transactions.stream().filter(c -> c instanceof Cliente).count();
		long sellerQty = transactions.stream().filter(s -> s instanceof Vendedor).count();
		String expensiveSaleId = getExpensiveSaleId();
		String sellerWorst = getWorstSeller();
		return new String(customerQty+"�"+sellerQty+"�"+expensiveSaleId+"�"+sellerWorst);
	}

	private String getExpensiveSaleId() {
		List<Venda> vendas = transactions.stream().filter(v -> v instanceof Venda).map(v -> (Venda) v)
				.collect(Collectors.toList());
		BigDecimal price = BigDecimal.ZERO;
		String saleId = "";
		for (Venda venda : vendas) {
			BigDecimal purchaseTotal = purchaseTotal(venda);
			if (price.compareTo(purchaseTotal) <= 0) {
				saleId = venda.getSalesId();
				price = purchaseTotal;
			}
		}
		return saleId;
	}

	private BigDecimal purchaseTotal(Venda venda) {
		BigDecimal valorTotal = BigDecimal.ZERO;
		for (Item item : venda.getItems()) {
			valorTotal = valorTotal.add(new BigDecimal(item.getPrice()));
		}
		return valorTotal;
	}

	private String getWorstSeller() {
		List<Venda> vendas = transactions.stream().filter(v -> v instanceof Venda).map(v -> (Venda) v).collect(Collectors.toList());
		BigDecimal minPrice = purchaseTotal(vendas.get(0));
		Venda venda= vendas.get(0);
		for (Venda v : vendas) {
			if (minPrice.compareTo(purchaseTotal(v)) < 0) {
			} else {
				minPrice = purchaseTotal(v);
				venda = v;
			}
		}
		return venda.getSalesName();
	}

}
