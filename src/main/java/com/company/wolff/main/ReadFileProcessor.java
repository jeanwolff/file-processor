package com.company.wolff.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;

import com.company.wolff.service.FileService;

public class ReadFileProcessor {

	public static void main(String[] args) throws IOException, InterruptedException {

		FileService service = new FileService();
		WatchService watchService = FileSystems.getDefault().newWatchService();

		Path inputPath = Paths.get(System.getProperty("user.home").concat(File.separator).concat("data")
				.concat(File.separator).concat("in"));
		Path outputPath = Paths.get(System.getProperty("user.home").concat(File.separator).concat("data")
				.concat(File.separator).concat("out"));

		inputPath.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);

		WatchKey watchKey;
		while ((watchKey = watchService.take()) != null) {

			for (WatchEvent<?> event : watchKey.pollEvents()) {

				String fileName = event.context().toString();
				System.out.println("Iniciando processamento do arquivo: " + fileName);

				if (fileName.toLowerCase().contains(".dat")) {
					Path inputFilePath = inputPath.resolve((Path) event.context());
					Path outputFilePath = outputPath.resolve(fileName.replace(".dat", ".done.dat"));

					List<String> lines = Files.readAllLines(inputFilePath);
					service.processTransactionFile(lines);

					try (BufferedWriter bf = Files.newBufferedWriter(outputFilePath)) {
						bf.write(service.writeReportOut());
					}
					System.out.println("finalizando processamento do arquivo: " + fileName);
				}
			}
			watchKey.reset();
		}

	}

}
