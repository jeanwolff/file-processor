package com.company.wolff.factory;

import com.company.wolff.pojo.Transaction;

public interface TransactionFactory {

	abstract Transaction createTransaction(String transaction);
}
