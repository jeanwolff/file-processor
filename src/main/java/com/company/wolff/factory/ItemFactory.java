package com.company.wolff.factory;

import com.company.wolff.pojo.Item;
import com.company.wolff.pojo.Transaction;

public class ItemFactory implements TransactionFactory {

	@Override
	public Transaction createTransaction(String transaction) {
		String[] itemSplit = transaction.split("-");
		return new Item(itemSplit[0], itemSplit[1], Float.parseFloat(itemSplit[2]));
	}

}
