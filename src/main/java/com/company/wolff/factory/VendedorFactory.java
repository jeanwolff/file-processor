package com.company.wolff.factory;

import com.company.wolff.pojo.Transaction;
import com.company.wolff.pojo.Vendedor;

public class VendedorFactory implements TransactionFactory {

	@Override
	public Transaction createTransaction(String transaction) {
		String[] vendedorSplit = transaction.split("�");
		return new Vendedor(vendedorSplit[0], vendedorSplit[1], vendedorSplit[2], Float.parseFloat(vendedorSplit[3]));
	}

}
