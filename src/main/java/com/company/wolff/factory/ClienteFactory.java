package com.company.wolff.factory;

import com.company.wolff.pojo.Cliente;
import com.company.wolff.pojo.Transaction;

public class ClienteFactory implements TransactionFactory {

	@Override
	public Transaction createTransaction(String transaction) {
		String[] clienteSplit = transaction.split("�");
		return new Cliente(clienteSplit[0], clienteSplit[1], clienteSplit[2], clienteSplit[3]);
	}

}
