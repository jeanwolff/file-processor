package com.company.wolff.factory;

import java.util.ArrayList;
import java.util.List;

import com.company.wolff.pojo.Item;
import com.company.wolff.pojo.Transaction;
import com.company.wolff.pojo.Venda;

public class VendaFactory implements TransactionFactory {

	@Override
	public Transaction createTransaction(String transaction) {
		String[] vendasSplit = transaction.split("�");
		return new Venda(vendasSplit[0], vendasSplit[1], vendasSplit[3], filterItemsTransaction(vendasSplit[2]));
	}

	private List<Item> filterItemsTransaction(String itemTransaction) {
		List<Item> items = new ArrayList<Item>();
		String[] itemsSplit = itemTransaction.replace("[", "").replace("]", "").split(","); 
		TransactionFactory itemFactory = new ItemFactory();
		  for(String item : itemsSplit) {
			  items.add((Item) itemFactory.createTransaction(item));
	        }
	        return items;
	}

	
}
